# MongoDB Shell Script Runner

## About

### Author

Ryan E. Anderson

---

### Description

This script can be used to execute commands for interacting with an instance of MongoDB.

---

### Version

0.1.0

---

### License

MIT

---

## Using the Script

Initially, the script will contain the following three variables, none of which has been set: connection_string, script, 
and output. Those variables need to be set in order for the script to execute properly.

### Variables

#### connection_string

A valid connection string must be assigned to the connection_string variable. A URI for connecting to a MongoDB instance 
must follow either the standard format or the format that uses DNS to construct a list of available servers (The scheme 
of the former is mongodb, and that of the latter is mongodb+srv.).

#### script

The path to a JavaScript file that contains shell commands for interacting with an instance of MongoDB must be assigned 
to the script variable.

Below is a sample of JavaScript code that contains commands for interacting with an instance of MongoDB.

```javascript
let matchStage = {
    $match: {
        startTime: {$gte: ISODate("2021-01-04T00:00:00Z")}
    }
};

let pipeline = [matchStage];

db.startup_log.aggregate(pipeline).pretty();
```

#### output

A path to a file that will be used to store the output from executing shell commands for interacting with an instance of 
MongoDB must be assigned to the output variable.

### Example

Below is an example that demonstrates how to assign values to the variables of the Bash shell script.

```text
connection_string=mongodb://localhost:27017/local
script=./js/aggregate.js
output=./txt/aggregation.txt
```
