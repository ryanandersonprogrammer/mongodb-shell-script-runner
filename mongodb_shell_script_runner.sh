#!/bin/bash

: <<'COMMENT'
The MIT License (MIT)

Copyright (c) 2021 Ryan Anderson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
COMMENT

: <<'COMMENT'
MongoDB Shell Script Runner v0.1.0

Copyright (C) 2021 Ryan E. Anderson
COMMENT

connection_string= # Assign a connection string that follows either the standard format or the format that uses DNS to construct a list of available servers (The scheme of the former is mongodb, and that of the latter is mongodb+srv.).
script=            # Assign the path to a JavaScript file that contains shell commands for interacting with an instance of MongoDB.
output=            # Assign the path to a file that will be used to store the output from executing shell commands for interacting with an instance of MongoDB.

if test -f $script && test -f $output; then
  mongo "$connection_string" <$script >$output

  status=$?

  printf "The mongo command exited with %d.\n\n" $status

  if [[ $status -eq 0 ]]; then
    printf "Output was created.\n\n"
  else
    printf "Output was not created. Please, ensure that the format of the connection string is valid.\n\n"
  fi
else
  printf "Output was not created. Please, ensure that the provided file paths are valid.\n\n"
fi

read -t 30 -n 1 -s -r -p "Press any key to continue..."
