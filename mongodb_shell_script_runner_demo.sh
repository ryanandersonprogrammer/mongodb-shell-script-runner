#!/bin/bash

: <<'COMMENT'
The MIT License (MIT)

Copyright (c) 2021 Ryan Anderson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
COMMENT

: <<'COMMENT'
MongoDB Shell Script Runner v0.1.0 Demo

Copyright (C) 2021 Ryan E. Anderson
COMMENT

connection_string=mongodb://localhost:27017/local # Assign a connection string that follows the standard format.
script=./js/aggregate.js                          # Assign the path to a file that contains a command for executing an aggregation pipeline.
output=./txt/aggregation.txt                      # Assign the path to a file that will store the output from executing an aggregation pipeline.

if test -f $script && test -f $output; then
  mongo "$connection_string" <$script >$output

  status=$?

  printf "The mongo command exited with %d.\n\n" $status

  if [[ $status -eq 0 ]]; then
    printf "Output was created.\n\n"
  else
    printf "Output was not created. Please, ensure that the format of the connection string is valid.\n\n"
  fi
else
  printf "Output was not created. Please, ensure that the provided file paths are valid.\n\n"
fi

read -t 30 -n 1 -s -r -p "Press any key to continue..."
